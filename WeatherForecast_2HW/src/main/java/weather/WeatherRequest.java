package main.java.weather;

public class WeatherRequest 
{
	private String cityName;
	private String countryCode;
	private String units;
	
	public WeatherRequest(String cityName, String units)
	{
		this.cityName = cityName;
		this.units = units;
	}

	public WeatherRequest(String cityName, String countryCode, String units)
	{
		this( cityName, units);
		this.countryCode = countryCode;
	}

	public String getLocationString()
	{
		if ( countryCode != null && !countryCode.isEmpty() )
		{
			return cityName + "," + countryCode;
		}
		return this.cityName;
	}
	
	public String getUnits() 
	{
		return units;
	}

	public void setUnits(String units) 
	{
		this.units = units;
	}

	public String getCityName()
	{
		return cityName;
	}

	public void setCityName(String cityName) 
	{
		this.cityName = cityName;
	}

	public String getCountryCode()
	{
		return countryCode;
	}

	public void setCountryCode(String countryCode) 
	{
		this.countryCode = countryCode;
	}

}
