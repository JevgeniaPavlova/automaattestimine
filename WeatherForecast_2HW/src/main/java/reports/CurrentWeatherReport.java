package main.java.reports;


public class CurrentWeatherReport extends WeatherReport 
{
	private String currentTemperature;

	public  String getCurrentTemperature() 
	{
		return currentTemperature;
	}

	public void setCurrentTemperature( String currentTemperature) 
	{
		this.currentTemperature = currentTemperature;
	}
}
