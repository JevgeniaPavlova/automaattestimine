package main.java.reports;

import java.util.Date;
import java.util.Map;

public class ForecastWeatherReport extends WeatherReport 
{
	private Map<Date, String[]> forecast;

	public Map<Date, String[]> getForecast() 
	{
		return forecast;
	}

	public void setForecast(Map<Date, String[]> forecast) 
	{
		this.forecast = forecast;
	}


}
