package test.java.repositories;

import static org.junit.Assert.*;


import org.junit.Test;

import main.java.reports.CurrentWeatherReport;
import main.java.repositories.CurrentWeatherRepository;
import main.java.weather.WeatherRequest;

public class CurrentWeatherRepositoryTest 
{
	@Test
	public void testIfWeatherRepositoryRespCityEqualsReqCity() 
	{ 
		try
		{
			CurrentWeatherRepository repository = new CurrentWeatherRepository();
			WeatherRequest request = new WeatherRequest("Tallinn", "EE", "metric");
			CurrentWeatherReport report = repository.getCurrentWeather(request);
			assertEquals(report.getCityName().toLowerCase(), request.getCityName().toLowerCase());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfWeatherRepositoryRespExistsWithoutCountryCode() 
	{ 
		try
		{
			CurrentWeatherRepository repository = new CurrentWeatherRepository();
			WeatherRequest request = new WeatherRequest("Tallinn", "metric");
			CurrentWeatherReport report = repository.getCurrentWeather(request);
			assertEquals(report.getCityName().toLowerCase(), request.getCityName().toLowerCase());
			assertNotNull(report.getCurrentTemperature());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfCurrentTemperaturExists() 
	{
		try
		{
			CurrentWeatherRepository repository = new CurrentWeatherRepository();
			WeatherRequest request = new WeatherRequest("Tallinn", "EE", "metric");
			CurrentWeatherReport report = repository.getCurrentWeather(request);
			assertNotNull(report.getCurrentTemperature());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}

	@Test
	public void testIfCurrentTemperaturDoesNotExist() 
	{
		try
		{
			CurrentWeatherRepository repository = new CurrentWeatherRepository();
			WeatherRequest noSuchLocationRequest = new WeatherRequest("1213132424", "metric");
			CurrentWeatherReport report = repository.getCurrentWeather(noSuchLocationRequest);
			assertNull(report.getCurrentTemperature());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfCurrentTemperaturIsInRange() 
	{
		try
		{
			//https://en.wikipedia.org/wiki/List_of_weather_records
			//with some reserve for new records =)
			double possibleMax = 100;
			double possibleMin = -100;
			CurrentWeatherRepository repository = new CurrentWeatherRepository();
			WeatherRequest request = new WeatherRequest("Tallinn", "EE", "metric");
			CurrentWeatherReport report = repository.getCurrentWeather(request);
			assertTrue(Double.valueOf(report.getCurrentTemperature()).compareTo(possibleMax) <= 0 && Double.valueOf(report.getCurrentTemperature()).compareTo(possibleMin) >= 0);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
}
