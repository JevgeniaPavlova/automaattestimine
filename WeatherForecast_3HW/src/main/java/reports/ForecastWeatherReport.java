package main.java.reports;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import main.java.repositories.WeatherRepository;

public class ForecastWeatherReport extends WeatherReport 
{
	private Map<Date, String[]> forecast;

	public Map<Date, String[]> getForecast() 
	{
		return forecast;
	}

	public void setForecast(Map<Date, String[]> forecast) 
	{
		this.forecast = forecast;
	}

	@Override
	public String toString() 
	{
		String reportRecord = "FORECAST FOR THREE DAYS report.\n\nCity: " + getCityName() + ".";
		Map<Date, String[]> dateTemperatures = getForecast();
		
		for (Map.Entry<Date, String[]> entry : dateTemperatures.entrySet())
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat(WeatherRepository.DATE_FORMAT);
			reportRecord += "\n\nDate: " +  dateFormat.format(entry.getKey()) + ".";
			String[] temperatures = entry.getValue();
			if( temperatures.length == 3 )
			{
				reportRecord += "\nMinimum temperature: " + temperatures[0] + ".";
				reportRecord += "\nMaximum temperature: " + temperatures[1] + ".";
				reportRecord += "\nAverage temperature: " + temperatures[2] + ".";
			}
		
		}
	
		return reportRecord;
	}
}
