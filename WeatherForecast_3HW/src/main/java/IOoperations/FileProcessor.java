package main.java.IOoperations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;

import main.java.reports.WeatherReport;

public class FileProcessor 
{
	protected final static String INPUT_FILE_NAME = "input.txt";
	protected final static String OUTPUT_FILE_NAME = "output.txt";
	
	public String readFromFile(String fileName) 
	{
		String fileValue = null;
		try(BufferedReader br = new BufferedReader(new FileReader(fileName)))
		{
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) 
		    {
		        sb.append(line);
		        line = br.readLine();
		    }
		    fileValue = sb.toString();
		    
		}catch (FileNotFoundException ex )
		{
			System.err.println("File was not found!");
		}catch (Exception ex )
		{
			System.err.println("Something went wrong while reading the input file!");
		}
		return  fileValue;
	}
	
	public boolean writeReportToFile(WeatherReport report)
	{
		boolean success = false;
		
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(new File(OUTPUT_FILE_NAME))))
		{
			bw.write(report.toString());
			success = true;
		}catch (Exception ex )
		{
			System.err.println("Something went wrong while writing the report to the file!");
		}

		return success;
	}
}
