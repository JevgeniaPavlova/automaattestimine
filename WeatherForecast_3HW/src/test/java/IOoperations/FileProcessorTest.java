package test.java.IOoperations;

import static org.junit.Assert.assertEquals;


import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

import main.java.IOoperations.FileProcessor;
import main.java.reports.CurrentWeatherReport;
import main.java.reports.ForecastWeatherReport;
import main.java.reports.WeatherReport;
import main.java.repositories.CurrentWeatherRepository;
import main.java.repositories.ForecastWeatherRepository;
import main.java.repositories.WeatherRepository;
import main.java.weather.WeatherRequest;
import static org.mockito.Mockito.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FileProcessorTest 
{
	@Test 
	public void testIfCurrentWeatherReportIsWrittenToFileWOError()
	{   
		try
		{
			// create mock 
			CurrentWeatherRepository weatherRepositoryMock = mock(CurrentWeatherRepository.class);
			// create class to test [given] 
			FileProcessor fileProcessor = new FileProcessor();
			// stub the mock
			CurrentWeatherReport report = new CurrentWeatherReport();
			report.setCityName("Tallinn");
			report.setCurrentTemperature("0");
			when(weatherRepositoryMock.getCurrentWeather(any(WeatherRequest.class))).thenReturn(report);
			// [when] invoke the test class 
			WeatherRequest request = new WeatherRequest("Tallinn", "EE", "metric");
			fileProcessor.writeReportToFile(weatherRepositoryMock.getCurrentWeather(request));
			// [then] test the behaviour when after invocation 
			assertEquals("CURRENT WEATHER report.City: Tallinn.Current temperature: 0.", fileProcessor.readFromFile("output.txt"));
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test 
	public void testIfForecastWeatherReportIsWrittenToFileWOError()
	{   
		try
		{
			// create mock 
			ForecastWeatherRepository weatherRepositoryMock = mock(ForecastWeatherRepository.class);
			// create class to test [given] 
			FileProcessor fileProcessor = new FileProcessor();
			// stub the mock
			ForecastWeatherReport report = new ForecastWeatherReport();
			report.setCityName("Tallinn");
			Map<Date, String[]> forecast = new HashMap<>();
			String[] temperaturesForDay1 = {"0", "1", "1"};
			String[] temperaturesForDay2 = {"0", "0", "0"};
			String[] temperaturesForDay3 = {"3", "5", "4"};

			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
			Date inputDate1 = dateFormat.parse("27.10.2017");
			Date inputDate2 = dateFormat.parse("28.10.2017");
			Date inputDate3 = dateFormat.parse("29.10.2017");
			
			forecast.put(inputDate1, temperaturesForDay1);
			forecast.put(inputDate2, temperaturesForDay2);
			forecast.put(inputDate3, temperaturesForDay3);
			report.setForecast(forecast);
			
			when(weatherRepositoryMock.getForecastWeatherForThreeDays(any(WeatherRequest.class))).thenReturn(report);
			// [when] invoke the test class 
			WeatherRequest request = new WeatherRequest("Tallinn", "metric");
			fileProcessor.writeReportToFile(weatherRepositoryMock.getForecastWeatherForThreeDays(request));
			// [then] test the behaviour when after invocation 
			assertEquals("FORECAST FOR THREE DAYS report.City: Tallinn.Date: 29.10.2017.Minimum temperature: 3.Maximum temperature: 5.Average temperature: 4.Date: 27.10.2017.Minimum temperature: 0.Maximum temperature: 1.Average temperature: 1.Date: 28.10.2017.Minimum temperature: 0.Maximum temperature: 0.Average temperature: 0.", fileProcessor.readFromFile("output.txt"));
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testSizeOfParametersFromFile() 
	{
		try
		{
			//assume that input.txt file has following value: Tallinn, forecast
			FileProcessor fileProcessor = new FileProcessor();
			String parametersString = fileProcessor.readFromFile("input.txt");
			String[] parameters = parametersString.split(",");
			int expectedSize = 2;
			assertEquals(expectedSize, parameters.length);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}

	@Test
	public void testNoSuchFileCase() 
	{
		try
		{
			FileProcessor fileProcessor = new FileProcessor();
			String parametersString = fileProcessor.readFromFile("nosuchfile.txt");
			assertNull( parametersString );
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testOnlyOneParamInFile() 
	{
		try
		{
			FileProcessor fileProcessor = new FileProcessor();
			String parametersString = fileProcessor.readFromFile("inputOneParamTest.txt");
			String[] parameters = parametersString.split(",");
			int expectedSize = 1;
			assertEquals(expectedSize, parameters.length);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfForecastWeatherForThreeDaysReportWasSuccessfullyCreated() 
	{
		try
		{
			WeatherReport report = WeatherRepository.getForecastWeatherForThreeDaysReport("Tallinn");
			FileProcessor fileProcessor = new FileProcessor();
			boolean isReportWasSuccessfullyCreated = fileProcessor.writeReportToFile(report);
			assertTrue(isReportWasSuccessfullyCreated);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfCurrentWeatherReportWasSuccessfullyCreated() 
	{
		try
		{
			WeatherReport report = WeatherRepository.getCurrentWeatherReport("Tallinn");
			FileProcessor fileProcessor = new FileProcessor();
			boolean isReportWasSuccessfullyCreated = fileProcessor.writeReportToFile(report);
			assertTrue(isReportWasSuccessfullyCreated);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
}
