package test.java.repositories;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import main.java.reports.ForecastWeatherReport;
import main.java.repositories.ForecastWeatherRepository;

import main.java.weather.WeatherRequest;

public class ForecastWeatherRepositoryMockTest 
{
	static ForecastWeatherRepository weatherRepositoryMock = null;
	static
	{
		try 
		{
			weatherRepositoryMock = mock(ForecastWeatherRepository.class);
			ForecastWeatherReport report = new ForecastWeatherReport();
			report.setCityName("Bali");
			Map<Date, String[]> forecast = new LinkedHashMap<>();
			String[] temperaturesForDay1 = {"22", "26", "24"};
			String[] temperaturesForDay2 = {"26", "28", "27"};
			String[] temperaturesForDay3 = {"20", "22", "21"};
	
			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
			Date inputDate1 = dateFormat.parse("27.11.2017");
			Date inputDate2 = dateFormat.parse("28.11.2017");
			Date inputDate3 = dateFormat.parse("29.11.2017");
			
			
			forecast.put(inputDate1, temperaturesForDay1);
			forecast.put(inputDate2, temperaturesForDay2);
			forecast.put(inputDate3, temperaturesForDay3);
			report.setForecast(forecast);
			when(weatherRepositoryMock.getForecastWeatherForThreeDays(any(WeatherRequest.class))).thenReturn(report);
		} catch (ParseException e) 
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public void testIfWeatherRepositoryRespCityEqualsReqCity() 
	{ 
		try
		{
			WeatherRequest request = new WeatherRequest("Bali", "ID", "metric");
			ForecastWeatherReport report = weatherRepositoryMock.getForecastWeatherForThreeDays(request);
			assertEquals(report.getCityName().toLowerCase(), request.getCityName().toLowerCase());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}

	@Test
	public void testIfWeatherRepositoryRespExistsWithoutCountryCode() 
	{ 
		try
		{
			WeatherRequest request = new WeatherRequest("Bali", "ID", "metric");
			ForecastWeatherReport report = weatherRepositoryMock.getForecastWeatherForThreeDays(request);
			assertEquals(report.getCityName().toLowerCase(), request.getCityName().toLowerCase());
			assertNotNull(report.getForecast());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}

	@Test
	public void testIfResultExists() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Bali", "ID", "metric");
			ForecastWeatherReport report = weatherRepositoryMock.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateForecast = report.getForecast();
			assertNotNull(dateForecast);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testSizeOfDays() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Bali", "ID", "metric");
			ForecastWeatherReport report = weatherRepositoryMock.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateForecast = report.getForecast();
			int expectedSize = 3;
			assertEquals(expectedSize, dateForecast.keySet().size());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testSizeOfTemperaturesValues() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Bali", "ID", "metric");
			ForecastWeatherReport report = weatherRepositoryMock.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateTemperatures = report.getForecast();
			Collection<String[]> temperatures = dateTemperatures.values();

			Iterator<String[]> iterator = temperatures.iterator();
			while( iterator.hasNext() )
			{
				String[] minMaxAvgTemperaturesOfTheDay = iterator.next();
				int expectedSize = 3;
				assertEquals(expectedSize, minMaxAvgTemperaturesOfTheDay.length);
			}
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfMinValueIsLessOrEqualThanMaxValue() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Bali", "ID", "metric");
			ForecastWeatherReport report = weatherRepositoryMock.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateTemperatures = report.getForecast();
			Collection<String[]> temperatures = dateTemperatures.values();
			Iterator<String[]> iterator = temperatures.iterator();
			while( iterator.hasNext() )
			{
				String[] minMaxAvgTemperaturesOfTheDay = iterator.next();
				assertTrue(Double.valueOf(minMaxAvgTemperaturesOfTheDay[0]) <= Double.valueOf(minMaxAvgTemperaturesOfTheDay[1]));
			}
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfMaxValueIsGreaterOrEqualThanMinValue() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Bali", "ID", "metric");
			ForecastWeatherReport report = weatherRepositoryMock.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateTemperatures = report.getForecast();
			Collection<String[]> temperatures = dateTemperatures.values();

			Iterator<String[]> iterator = temperatures.iterator();
			while( iterator.hasNext() )
			{
				String[] minMaxAvgTemperaturesOfTheDay = iterator.next();
				assertTrue(Double.valueOf(minMaxAvgTemperaturesOfTheDay[1])>= Double.valueOf(minMaxAvgTemperaturesOfTheDay[0]));
			}
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
}
