package test.java.repositories;


import static org.junit.Assert.*;

import org.junit.Test;

import main.java.reports.CurrentWeatherReport;
import main.java.reports.ForecastWeatherReport;
import main.java.reports.WeatherReport;
import main.java.repositories.WeatherRepository;

public class WeatherRepositoryTest 
{

	@Test
	public void testIfCurrentWeatherReportIsNotNull() 
	{
		try
		{
			WeatherReport report = WeatherRepository.getCurrentWeatherReport("Tallinn");
			assertNotNull(report);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfCurrentWeatherReportIsNeededType() 
	{
		try
		{
			WeatherReport report = WeatherRepository.getCurrentWeatherReport("Tallinn");
			assertTrue(report instanceof CurrentWeatherReport);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfForecastWeatherReportIsNotNull() 
	{
		try
		{
			WeatherReport report = WeatherRepository.getForecastWeatherForThreeDaysReport("Tallinn");
			assertNotNull(report);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfForecastWeatherReportIsNeededType() 
	{
		try
		{
			WeatherReport report = WeatherRepository.getForecastWeatherForThreeDaysReport("Tallinn");
			assertTrue(report instanceof ForecastWeatherReport);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
}