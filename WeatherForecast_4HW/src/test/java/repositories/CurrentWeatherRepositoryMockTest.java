package test.java.repositories;

import static org.junit.Assert.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Test;
import main.java.reports.CurrentWeatherReport;
import main.java.repositories.CurrentWeatherRepository;
import main.java.weather.WeatherRequest;

public class CurrentWeatherRepositoryMockTest 
{	static CurrentWeatherRepository weatherRepositoryMock = null;	static	{		weatherRepositoryMock = mock(CurrentWeatherRepository.class);		CurrentWeatherReport report = new CurrentWeatherReport();		report.setCityName("London");		report.setCurrentTemperature("10");		when(weatherRepositoryMock.getCurrentWeather(any(WeatherRequest.class))).thenReturn(report);	}	
	@Test
	public void mockTestIfWeatherRepositoryRespCityEqualsReqCity() 
	{ 
		try
		{			WeatherRequest request = new WeatherRequest("London", "10");
			CurrentWeatherReport report = weatherRepositoryMock.getCurrentWeather(request);
			assertEquals(report.getCityName().toLowerCase(), request.getCityName().toLowerCase());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void mockTestIfWeatherRepositoryRespExistsWithoutCountryCode() 
	{ 
		try
		{
			WeatherRequest request = new WeatherRequest("London", "10");			CurrentWeatherReport report = weatherRepositoryMock.getCurrentWeather(request);
			assertEquals(report.getCityName().toLowerCase(), request.getCityName().toLowerCase());
			assertNotNull(report.getCurrentTemperature());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void mockTestIfCurrentTemperaturExists() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("London", "10");			CurrentWeatherReport report = weatherRepositoryMock.getCurrentWeather(request);
			assertNotNull(report.getCurrentTemperature());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}

	@Test
	public void mockTestIfCurrentTemperaturIsInRange() 
	{
		try
		{
			//https://en.wikipedia.org/wiki/List_of_weather_records
			//with some reserve for new records =)
			double possibleMax = 100;
			double possibleMin = -100;
			WeatherRequest request = new WeatherRequest("London", "10");			CurrentWeatherReport report = weatherRepositoryMock.getCurrentWeather(request);
			assertTrue(Double.valueOf(report.getCurrentTemperature()).compareTo(possibleMax) <= 0 && Double.valueOf(report.getCurrentTemperature()).compareTo(possibleMin) >= 0);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
}
