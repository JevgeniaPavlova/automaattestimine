package test.java.IOoperations;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

import main.java.IOoperations.FileProcessor;
import main.java.reports.CurrentWeatherReport;
import main.java.reports.ForecastWeatherReport;
import main.java.reports.WeatherReport;
import main.java.repositories.CurrentWeatherRepository;
import main.java.repositories.ForecastWeatherRepository;
import main.java.repositories.WeatherRepository;
import main.java.weather.WeatherRequest;
import static org.mockito.Mockito.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FileProcessorTest 
{
	@Test 
	public void testIfCurrentWeatherReportIsWrittenToFileWOError()
	{   
		try
		{
			// create mock 
			CurrentWeatherRepository weatherRepositoryMock = mock(CurrentWeatherRepository.class);
			// create class to test [given] 
			FileProcessor fileProcessor = new FileProcessor();
			// stub the mock
			CurrentWeatherReport report = new CurrentWeatherReport();
			report.setCityName("Test");
			report.setCurrentTemperature("0");
			when(weatherRepositoryMock.getCurrentWeather(any(WeatherRequest.class))).thenReturn(report);
			// [when] invoke the test class 
			WeatherRequest request = new WeatherRequest("Test", "metric");
			Files.deleteIfExists(Paths.get(request.getCityName() + ".txt"));
			fileProcessor.writeReportToFile(weatherRepositoryMock.getCurrentWeather(request));
			// [then] test the behaviour when after invocation 
			List<String> fileContentByLines = fileProcessor.readFromFile(request.getCityName() + ".txt");
			fileContentByLines.removeAll(Arrays.asList("", null));
			//query time + capture + city name + current temp = 4 strings
			assertEquals(4, fileContentByLines.size());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test 
	public void testIfForecastWeatherReportIsWrittenToFileWOError()
	{   
		try
		{
			// create mock 
			ForecastWeatherRepository weatherRepositoryMock = mock(ForecastWeatherRepository.class);
			// create class to test [given] 
			FileProcessor fileProcessor = new FileProcessor();
			// stub the mock
			ForecastWeatherReport report = new ForecastWeatherReport();
			report.setCityName("Tallinn");
			Map<Date, String[]> forecast = new LinkedHashMap<>();
			String[] temperaturesForDay1 = {"0", "1", "1"};
			String[] temperaturesForDay2 = {"0", "0", "0"};
			String[] temperaturesForDay3 = {"3", "5", "4"};

			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
			Date inputDate1 = dateFormat.parse("27.10.2017");
			Date inputDate2 = dateFormat.parse("28.10.2017");
			Date inputDate3 = dateFormat.parse("29.10.2017");
			
			forecast.put(inputDate1, temperaturesForDay1);
			forecast.put(inputDate2, temperaturesForDay2);
			forecast.put(inputDate3, temperaturesForDay3);
			report.setForecast(forecast);
			
			when(weatherRepositoryMock.getForecastWeatherForThreeDays(any(WeatherRequest.class))).thenReturn(report);
			// [when] invoke the test class 
			WeatherRequest request = new WeatherRequest("Tallinn", "metric");
			Files.deleteIfExists(Paths.get(request.getCityName() + ".txt"));
			fileProcessor.writeReportToFile(weatherRepositoryMock.getForecastWeatherForThreeDays(request));
			List<String> fileContentByLines = fileProcessor.readFromFile(request.getCityName() + ".txt");
			fileContentByLines.removeAll(Arrays.asList("", null));
			// [then] test the behaviour when after invocation 
			fileContentByLines.removeAll(Arrays.asList("", null));
			//query time + capture + city name + (date capture + min temp + max temp + avg temp)*3 = 15 strings
			assertEquals(15, fileContentByLines.size());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testSizeOfParametersFromFile() 
	{
		try
		{
			//assume that input.txt file has following value: Tallinn, forecast
			FileProcessor fileProcessor = new FileProcessor();
			String parametersString = fileProcessor.readFromFile("input.txt").toString();
			String[] parameters = parametersString.split(",");
			int expectedSize = 2;
			assertEquals(expectedSize, parameters.length);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}

	@Test
	public void testNoSuchFileCase() 
	{
		try
		{
			FileProcessor fileProcessor = new FileProcessor();
			List<String> fileString = fileProcessor.readFromFile("nosuchfile.txt");
			assertEquals(0, fileString.size() );
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testOnlyOneParamInFile() 
	{
		try
		{
			FileProcessor fileProcessor = new FileProcessor();
			String parametersString = fileProcessor.readFromFile("inputOneParamTest.txt").toString();
			String[] parameters = parametersString.split(",");
			int expectedSize = 1;
			assertEquals(expectedSize, parameters.length);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfForecastWeatherForThreeDaysReportWasSuccessfullyCreated() 
	{
		try
		{
			WeatherReport report = WeatherRepository.getForecastWeatherForThreeDaysReport("Tallinn");
			FileProcessor fileProcessor = new FileProcessor();
			boolean isReportWasSuccessfullyCreated = fileProcessor.writeReportToFile(report);
			assertTrue(isReportWasSuccessfullyCreated);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfCurrentWeatherReportWasSuccessfullyCreated() 
	{
		try
		{
			WeatherReport report = WeatherRepository.getCurrentWeatherReport("Tallinn");
			FileProcessor fileProcessor = new FileProcessor();
			boolean isReportWasSuccessfullyCreated = fileProcessor.writeReportToFile(report);
			assertTrue(isReportWasSuccessfullyCreated);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
}
