package main.java.IOoperations;

import java.util.List;

import main.java.reports.WeatherReport;
import main.java.repositories.WeatherRepository;

public class InputProcessor 
{
	static ConsoleProcessor consoleProcessor = new ConsoleProcessor();
	static FileProcessor fileProcessor = new FileProcessor();
	
	public final static String INPUT_TYPE_FILE = "file";
	public final static String INPUT_TYPE_CONSOLE = "console";
	
	public final static String FORECAST_TYPE_FORECAST = "forecast";
	public final static String FORECAST_TYPE_CURRENT = "current";

	
	public static void startProgramm()
	{
		String inputType = consoleProcessor.askAboutConsoleOrFileInput();
		if ( inputType.toLowerCase().equals(INPUT_TYPE_FILE))
		{
			startProgrammWithInputFile();
		}else if( inputType.toLowerCase().equals(INPUT_TYPE_CONSOLE))
		{
			startProgrammWithConsole();	
		}else
		{
			System.err.println("Error in input type!");
		}
	}
	
	protected static void startProgrammWithConsole()
	{
		String city = consoleProcessor.askCityName();
		String forecastType = consoleProcessor.askForecastType();
		makeForecastAccordingToForecastType( city, forecastType );
	}
	
	protected static void startProgrammWithInputFile()
	{
		System.out.println("\n\nStart reading parameters from input.txt file. \nNote that it should be two parameters - city and forecast type. \nExample: 'Tallinn, current' or 'Tallinn, forecast'");
		List<String> readedStringsFromFile = fileProcessor.readFromFile(FileProcessor.INPUT_FILE_NAME);
		for( String cityString : readedStringsFromFile )
		{
			cityString = cityString.replaceAll("\\s","");
			String[] requestParameters = cityString.split(",");
			if( requestParameters == null )
			{
				System.err.println("No parameters were found in file!");
			}
			else if (  requestParameters.length  == 2 )
			{
				String city =  requestParameters[0];
				String forecastType = requestParameters[1];
				System.out.println("\n\nParameters are: City - " + city + ", Forecast type: " + forecastType);
				makeForecastAccordingToForecastType( city, forecastType );
			}
			else
			{
				System.err.println("In file should be two parameters!\nExample: 'Tallinn, current' or 'Tallinn, forecast'");
			}
		}
	}
	
	protected static void makeForecastAccordingToForecastType( String city, String forecastType )
	{
		WeatherReport report = null;
		if( forecastType.toLowerCase().equals(FORECAST_TYPE_FORECAST))
		{
			report = WeatherRepository.getForecastWeatherForThreeDaysReport( city );
		}else if ( forecastType.toLowerCase().equals(FORECAST_TYPE_CURRENT) )
		{
			report = WeatherRepository.getCurrentWeatherReport( city );
		}else
		{
			System.err.println("Error in forecast type!");
		}
		generateOutputFile( report );
	}
	
	protected static void generateOutputFile( WeatherReport report )
	{
		if( report != null )
		{
			boolean reportWasSuccessfullyWrittenToFile = fileProcessor.writeReportToFile(report);
			if ( reportWasSuccessfullyWrittenToFile )
			{
				System.out.println("Report file was successfully generated!");	
			}
			else
			{
				System.err.println("Error in output file generation!");
			}
		}
	}
}
