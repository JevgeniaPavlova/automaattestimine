package main.java.IOoperations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import main.java.reports.WeatherReport;

public class FileProcessor 
{
	protected final static String INPUT_FILE_NAME = "input.txt";
	protected final static String OUTPUT_FILE_SUFFIX = ".txt";
	protected final static String OUTPUT_FILE_NAME = "output.txt";
	protected final static String DATE_FORMAT = "dd.MM.yyyy HH:mm";
	
	public List<String> readFromFile(String fileName) 
	{
		List<String> fileValues = new ArrayList<String>();
		
		try(BufferedReader br = new BufferedReader(new FileReader(fileName)))
		{
		    String line = br.readLine();
		    while (line != null) 
		    {
		    	fileValues.add(line);
		        line = br.readLine();
		    }
		}catch (FileNotFoundException ex )
		{
			System.err.println("File was not found!");
		}catch (Exception ex )
		{
			System.err.println("Something went wrong while reading the input file!");
		}
		return  fileValues;
	}
	
	public boolean writeReportToFile(WeatherReport report)
	{
		boolean success = false;
		String fileName = makeFileName(report.getCityName());
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName), true)))
		{
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			Date date = new Date();
			bw.write("\n---------------QUERY TIME:" + dateFormat.format(date) + "---------------");
			bw.write(report.toString());
			success = true;
		}catch (Exception ex )
		{
			System.err.println("Something went wrong while writing the report to the file!");
		}

		return success;
	}

	private String makeFileName( String city ) 
	{
		if( !city.isEmpty() )
		{
			return city + OUTPUT_FILE_SUFFIX;
		}
		return OUTPUT_FILE_NAME;
	}
}
