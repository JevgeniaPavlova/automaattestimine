package main.java.IOoperations;

import java.util.Scanner;

public class ConsoleProcessor 
{
	private Scanner scanner = null;
	public ConsoleProcessor()
	{
		scanner = new Scanner(System.in);
	}
	public String askCityName()
	{
		String city = null;
		String prompt = "\n\nPlease enter the name of the city.\nExample: Tallinn\n";
		do 
		{
			System.out.print(prompt);
			city = scanner.nextLine();
		    prompt = "\nPlease enter a valid city.\nExample: Tallinn. \n";
		} while ( city.isEmpty());
		System.out.println("You have entered "+ city + " input type.");
		return city;
	}
	
	public String askAboutConsoleOrFileInput()
	{
		String inputType = null;
		String prompt = "\n\nPlease enter the method by which you want to make a request. \nExample: file (by input file)/ console\n";
		do {
			System.out.print(prompt);
			inputType = scanner.nextLine();
		    prompt = "\nPlease enter a valid request type.\nExample: file (using file)/ console (using console). \n";
		} while ( !inputType.toLowerCase().equals(InputProcessor.INPUT_TYPE_CONSOLE) && !inputType.toLowerCase().equals(InputProcessor.INPUT_TYPE_FILE));

		System.out.println("You have chosen "+ inputType + " input type.");
		return inputType;
	}
	
	public String askForecastType()
	{
		String forecastType = null;
		String prompt = "\n\nPlease enter the forecast type. \nExample: Forecast( for forecast for three days)/ Current (for current weather)\n";
		do 
		{
			System.out.print(prompt);
			forecastType = scanner.nextLine();
		    prompt = "\nPlease enter a valid forecast type.\nExample: Forecast( for forecast for three days)/ Current (for current weather). \n";
		} while ( !forecastType.toLowerCase().equals(InputProcessor.FORECAST_TYPE_CURRENT) && !forecastType.toLowerCase().equals(InputProcessor.FORECAST_TYPE_FORECAST));
		System.out.println("You have chosen "+ forecastType + " input type.");
		return forecastType;
	}

}
