package main.java.requestHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class JSONRequester 
{
	private static final String UTF_CHARSET = "UTF-8";
	
	public static JSONObject makeReqestAndGetJsonObject( String urlToRequest )
	{
		JSONObject responseInJsonFormat = null;
		if( !urlToRequest.isEmpty() )
		{
			InputStream inputStream = makeHttpCall( urlToRequest );
			if( inputStream != null )
			{
				responseInJsonFormat = parseResponse( inputStream );
			}
			
		}
		return responseInJsonFormat;
	}
	
	private static InputStream makeHttpCall(String urlToRequest)
	{
		InputStream inputStream = null;
		try 
		{
			URL weatherApiUrl = new URL(urlToRequest);
			URLConnection connection = weatherApiUrl.openConnection();
			inputStream = connection.getInputStream();
			
		} catch (IOException e) 
		{
			return null;
		}
		return inputStream;
	}
	
	private static JSONObject parseResponse( InputStream inputStream )
	{
		JSONObject jsonObject = null;
		JSONParser jsonParser = new JSONParser();
		try 
		{
			jsonObject = (JSONObject)jsonParser.parse( new InputStreamReader(inputStream, UTF_CHARSET));
		} catch (Exception  e) 
		{
			e.printStackTrace();
		}
		return jsonObject;
	}
}
