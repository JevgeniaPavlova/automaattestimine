package main.java.reports;


public class CurrentWeatherReport extends WeatherReport 
{
	private String currentTemperature;

	public  String getCurrentTemperature() 
	{
		return currentTemperature;
	}

	public void setCurrentTemperature( String currentTemperature) 
	{
		this.currentTemperature = currentTemperature;
	}
	
	@Override
	public String toString() 
	{
		return "\n\nCURRENT WEATHER report.\n\nCity: " + getCityName() + "." +  "\nCurrent temperature: " + getCurrentTemperature() + ".";
	}
}
