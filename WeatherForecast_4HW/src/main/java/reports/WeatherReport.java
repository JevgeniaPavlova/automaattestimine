package main.java.reports;


public class WeatherReport
{
	private String cityName;
	private String temperatureUnits;
	
	
	public String getCityName() 
	{
		return cityName;
	}

	public void setCityName(String cityName) 
	{
		this.cityName = cityName;
	}

	public String getTemperatureUnits() 
	{
		return temperatureUnits;
	}

	public void setTemperatureUnits(String temperatureUnits)
	{
		this.temperatureUnits = temperatureUnits;
	}
}
