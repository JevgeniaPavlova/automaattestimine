
----QUERY DATE:27.11.2017 14:22---------------

FORECAST FOR THREE DAYS report.

City: London.

Date: 27.11.2017.
Minimum temperature: 8.08.
Maximum temperature: 8.18.
Average temperature: 8.18.

Date: 28.11.2017.
Minimum temperature: 5.68.
Maximum temperature: 5.68.
Average temperature: 5.68.

Date: 29.11.2017.
Minimum temperature: 4.65.
Maximum temperature: 4.65.
Average temperature: 4.65.
---------------------------------------
----QUERY DATE:27.11.2017 14:42---------------

CURRENT WEATHER report.

City: London.
Current temperature: 9.82.
---------------------------------------