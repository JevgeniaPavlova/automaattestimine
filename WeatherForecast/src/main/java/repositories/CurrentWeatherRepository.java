package main.java.repositories;

import org.json.simple.JSONObject;

import main.java.reports.CurrentWeatherReport;
import main.java.reports.WeatherReport;
import main.java.requestHelper.WeatherRequester;
import main.java.weather.WeatherRequest;

public class CurrentWeatherRepository extends WeatherRepository 
{
	private final static String CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?";
	
	public CurrentWeatherReport getCurrentWeather( WeatherRequest weatherRequest )
	{
		String urlToRequest = CURRENT_WEATHER_URL + LOCATION_NAME_PARAMETER + "=" + weatherRequest.getLocationString() + "&" + UNITS_PARAMETER + "=" + weatherRequest.getUnits() + "&" + API_KEY_PARAMETER + "=" + API_KEY_VALUE;
		JSONObject receivedJsonObject = WeatherRequester.makeReqestAndGetJsonObject(urlToRequest);
		CurrentWeatherReport currentWeatherReport = (CurrentWeatherReport) createWeatherReport(receivedJsonObject);
		return currentWeatherReport;
	}

	@Override
	protected WeatherReport createWeatherReport(JSONObject receivedJsonObject) 
	{
		CurrentWeatherReport currentWeatherReport = new CurrentWeatherReport();
		if ( receivedJsonObject != null )
		{
			JSONObject mainInformation = (JSONObject) receivedJsonObject.get(MAIN_OBJECT);
			currentWeatherReport.setCityName(String.valueOf(receivedJsonObject.get(NAME_FIELD)));
			currentWeatherReport.setCurrentTemperature(String.valueOf(mainInformation.get(TEMP_FIELD)));
		}
		return currentWeatherReport;
	}
}
