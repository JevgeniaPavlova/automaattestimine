package main.java.repositories;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import main.java.reports.ForecastWeatherReport;
import main.java.reports.WeatherReport;
import main.java.requestHelper.WeatherRequester;
import main.java.weather.WeatherRequest;

public class ForecastWeatherRepository extends WeatherRepository
{
	private final static String CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/forecast?";
	Map<Date, String[]> forecast = new LinkedHashMap<>();
	
	public ForecastWeatherReport getForecastWeatherForThreeDays( WeatherRequest weatherRequest )
	{
		String urlToRequest = CURRENT_WEATHER_URL + LOCATION_NAME_PARAMETER + "=" + weatherRequest.getLocationString() + "&" + UNITS_PARAMETER + "=" + weatherRequest.getUnits() + "&" + API_KEY_PARAMETER + "=" + API_KEY_VALUE;
		JSONObject receivedJsonObject = WeatherRequester.makeReqestAndGetJsonObject(urlToRequest);
		ForecastWeatherReport forecastWeatherReport = (ForecastWeatherReport) createWeatherReport(receivedJsonObject);
		return forecastWeatherReport;
	}

	@Override
	protected WeatherReport createWeatherReport(JSONObject receivedJsonObject) 
	{
		ForecastWeatherReport forecastWeatherReport = new ForecastWeatherReport();
		if ( receivedJsonObject != null )
		{
			JSONObject cityInformation = (JSONObject) receivedJsonObject.get(CITY_OBJECT);
			forecastWeatherReport.setCityName(String.valueOf(cityInformation.get(NAME_FIELD)));
			getThreeDaysForecastWithMixAndMaxTemperature( receivedJsonObject );
			forecastWeatherReport.setForecast(forecast);
		}
		return forecastWeatherReport;
	}
	
	private Date parseDateFromString( String date )
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		Date parsedDate = null;
		try 
		{
			parsedDate = df.parse(date);
		} catch (ParseException e) 
		{
		    e.printStackTrace();
		}
		return parsedDate;
	}
	private boolean checkTheDatesIfTheyAreTheSameDay(Date date1, Date date2)
	{
		if( date1 != null && date2 != null )
		{
			SimpleDateFormat fmt = new SimpleDateFormat(DATE_FORMAT);
			return fmt.format(date1).equals(fmt.format(date2));
		}
		return false;
	}
	
	private void addValuesToDayForecast( JSONObject forecastForSpecificDate, Date date )
	{
		if( forecast.keySet().size() < 3 )
		{
			JSONObject mainInformation = (JSONObject) forecastForSpecificDate.get(MAIN_OBJECT);
			String[] minAndMaxTemperatures = { String.valueOf(mainInformation.get(TEMP_MIN_FIELD)), String.valueOf(mainInformation.get(TEMP_MAX_FIELD)), String.valueOf(mainInformation.get(TEMP_FIELD))};
			forecast.put(date, minAndMaxTemperatures);
		}
	}
	
	private void getThreeDaysForecastWithMixAndMaxTemperature( JSONObject receivedJsonObject )
	{
		JSONArray listOfForecastsOfDaysAndHours = (JSONArray) receivedJsonObject.get(LIST_OBJECT);
		int sizeOfList = listOfForecastsOfDaysAndHours.size();
		Date previousDateForComparison = null;
		for(int index = 0; index < sizeOfList; index++)
		{
			JSONObject forecastForSpecificDate = (JSONObject) listOfForecastsOfDaysAndHours.get(index);
			Date date = parseDateFromString( String.valueOf(forecastForSpecificDate.get(DATE_FIELD)) );
			boolean forecastForThisDayIsAlreadyExists = checkTheDatesIfTheyAreTheSameDay( date, previousDateForComparison);
			if ( !forecastForThisDayIsAlreadyExists )
			{	
				addValuesToDayForecast( forecastForSpecificDate, date );
			}
			previousDateForComparison = date;
		}
	}
}
