package main.java.repositories;

import org.json.simple.JSONObject;

import main.java.reports.WeatherReport;

public abstract class WeatherRepository 
{
	protected final static String LOCATION_NAME_PARAMETER = "q";
	protected final static String UNITS_PARAMETER = "units";
	protected final static String API_KEY_PARAMETER = "appid";
	
	protected final static String API_KEY_VALUE = "e958afb38979504888632122a1ac4ca5";
	protected final static String MAIN_OBJECT = "main";
	protected final static String LIST_OBJECT = "list";
	protected final static String CITY_OBJECT = "city";
	
	protected final static String NAME_FIELD = "name";
	protected final static String TEMP_FIELD = "temp";
	protected final static String TEMP_MIN_FIELD = "temp_min";
	protected final static String TEMP_MAX_FIELD = "temp_max";
	protected final static String DATE_FIELD = "dt_txt";
	
	protected final static String DATE_FORMAT = "dd.MM.yyyy";
	
	protected abstract WeatherReport createWeatherReport(JSONObject receivedJsonObject);
}
