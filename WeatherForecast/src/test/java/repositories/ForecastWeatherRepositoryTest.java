package test.java.repositories;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.junit.Test;

import main.java.reports.CurrentWeatherReport;
import main.java.reports.ForecastWeatherReport;
import main.java.repositories.CurrentWeatherRepository;
import main.java.repositories.ForecastWeatherRepository;

import main.java.weather.WeatherRequest;

public class ForecastWeatherRepositoryTest 
{
	@Test
	public void testIfWeatherRepositoryRespCityEqualsReqCity() 
	{ 
		try
		{
			WeatherRequest request = new WeatherRequest("Tallinn", "EE", "metric");
			ForecastWeatherRepository repository = new ForecastWeatherRepository();
			ForecastWeatherReport report = repository.getForecastWeatherForThreeDays(request);
			assertEquals(report.getCityName().toLowerCase(), request.getCityName().toLowerCase());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfCurrentReportDoesNotExistWithInvalidLocation() 
	{
		try
		{
			ForecastWeatherRepository repository = new ForecastWeatherRepository();
			WeatherRequest noSuchLocationRequest = new WeatherRequest("1213132424", "metric");
			ForecastWeatherReport report = repository.getForecastWeatherForThreeDays(noSuchLocationRequest);
			assertNull(report.getForecast());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfWeatherRepositoryRespExistsWithoutCountryCode() 
	{ 
		try
		{
			WeatherRequest request = new WeatherRequest("Tallinn", "metric");
			ForecastWeatherRepository repository = new ForecastWeatherRepository();
			ForecastWeatherReport report = repository.getForecastWeatherForThreeDays(request);
			assertEquals(report.getCityName().toLowerCase(), request.getCityName().toLowerCase());
			assertNotNull(report.getForecast());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}

	@Test
	public void testIfResultExists() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Tallinn", "metric");
			ForecastWeatherRepository repository = new ForecastWeatherRepository();
			ForecastWeatherReport report = repository.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateForecast = report.getForecast();
			assertNotNull(dateForecast);
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testSizeOfDays() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Tallinn", "metric");
			ForecastWeatherRepository repository = new ForecastWeatherRepository();
			ForecastWeatherReport report = repository.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateForecast = report.getForecast();
			int expectedSize = 3;
			assertEquals(expectedSize, dateForecast.keySet().size());
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testSizeOfTemperaturesValues() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Tallinn", "metric");
			ForecastWeatherRepository repository = new ForecastWeatherRepository();
			ForecastWeatherReport report = repository.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateTemperatures = report.getForecast();
			Collection<String[]> temperatures = dateTemperatures.values();

			Iterator<String[]> iterator = temperatures.iterator();
			while( iterator.hasNext() )
			{
				String[] minMaxAvgTemperaturesOfTheDay = iterator.next();
				int expectedSize = 3;
				assertEquals(expectedSize, minMaxAvgTemperaturesOfTheDay.length);
			}
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfMinValueIsLessOrEqualThanMaxValue() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Tallinn", "metric");
			ForecastWeatherRepository repository = new ForecastWeatherRepository();
			ForecastWeatherReport report = repository.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateTemperatures = report.getForecast();
			Collection<String[]> temperatures = dateTemperatures.values();
			Iterator<String[]> iterator = temperatures.iterator();
			while( iterator.hasNext() )
			{
				String[] minMaxAvgTemperaturesOfTheDay = iterator.next();
				assertTrue(Double.valueOf(minMaxAvgTemperaturesOfTheDay[0]) <= Double.valueOf(minMaxAvgTemperaturesOfTheDay[1]));
			}
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
	
	@Test
	public void testIfMaxValueIsGreaterOrEqualThanMinValue() 
	{
		try
		{
			WeatherRequest request = new WeatherRequest("Tallinn", "metric");
			ForecastWeatherRepository repository = new ForecastWeatherRepository();
			ForecastWeatherReport report = repository.getForecastWeatherForThreeDays(request);
			Map<Date, String[]> dateTemperatures = report.getForecast();
			Collection<String[]> temperatures = dateTemperatures.values();

			Iterator<String[]> iterator = temperatures.iterator();
			while( iterator.hasNext() )
			{
				String[] minMaxAvgTemperaturesOfTheDay = iterator.next();
				assertTrue(Double.valueOf(minMaxAvgTemperaturesOfTheDay[1])>= Double.valueOf(minMaxAvgTemperaturesOfTheDay[0]));
			}
		}catch(Exception e)
		{
			fail("Failure cause: " + e.getMessage());
		}
	}
}
